#include <stdlib.h>
#include <stdio.h>
#include <time.h>

double get_time_s(struct timespec start, struct timespec end)
{
    return (double)(end.tv_sec  - start.tv_sec) +
           (double)(end.tv_nsec - start.tv_nsec) / 1.0e9;
}

int main(int argc, char *argv[])
{
    size_t n = 100;
    size_t r = 100;

    if (argc >= 3) {
        if (sscanf(argv[1], "%zu", &n) != 1 ||
            sscanf(argv[2], "%zu", &r) != 1 ) {
            fprintf(stderr, "Error reading argument(s)\n");
            exit(1);
        }
    }

    printf("n = %zu\n", n);
    printf("r = %zu\n", r);

    #ifdef CGPU
        double frac_gpu = 0.5;
        if (argc >= 4) {
            sscanf(argv[3], "%lf", &frac_gpu);
        }
        const size_t ngpu = n*frac_gpu;
        printf("frac_gpu = %g\n", frac_gpu);
        printf("ngpu = %zu\n", ngpu);
        #define NOWAIT nowait
    #else
        #define NOWAIT
        const size_t ngpu = n;
    #endif

    const double alpha = 0.7;
    const double beta  = 0.3;

    double *x = malloc(n*sizeof(double)),
           *y = malloc(n*sizeof(double));

    double sum = 0.0;

    #ifndef GPU_PARALLEL_FOR
        #define GPU_PARALLEL_FOR parallel for simd
    #endif

    struct timespec start,end
    #ifdef CGPU
        ,cpuend
    #endif
    ;
    double time_init, time_main;
    #ifdef CGPU
        double time_main_cpu;
    #endif

    #if defined(GPU) || defined(CGPU)
        #pragma omp target enter data map(alloc:x[:ngpu],y[:ngpu])
        #pragma acc enter data pcreate(x[0:ngpu],y[0:ngpu])
    #endif

    // Fill with data
    #define INIT_DATA x[i] = 0.5*i/n; y[i] = 0.8*i/n

    clock_gettime(CLOCK_MONOTONIC, &start);
    #if defined(GPU) || defined(CGPU)
        #pragma omp target teams distribute GPU_PARALLEL_FOR
        #pragma acc parallel loop
    #elif defined(CPU)
        #pragma omp parallel for simd
    #endif
    for (size_t i = 0; i < ngpu; i++) {
        INIT_DATA;
    }
    #ifdef CGPU
        #pragma omp parallel for simd
        for (size_t i = ngpu; i < n; i++) {
            INIT_DATA;
        }
    #endif
    clock_gettime(CLOCK_MONOTONIC, &end);
    time_init = get_time_s(start,end);

    // Main code
    clock_gettime(CLOCK_MONOTONIC, &start);
    #define MAIN_BODY for (size_t j = 0; j < r; j++) { \
        y[i] = alpha*x[i] + beta*y[i]; \
    }

    sum = 0.0;
    #if defined(GPU) || defined(CGPU)
        #pragma omp target map(sum) NOWAIT
        #pragma omp teams distribute GPU_PARALLEL_FOR reduction(+:sum)
        #pragma acc parallel loop reduction(+:sum)
    #elif defined(CPU)
        #pragma omp parallel for simd reduction(+:sum)
    #endif
    for (size_t i = 0; i < ngpu; i++) {
        MAIN_BODY;
        sum = sum + y[i];
    }
    #ifdef CGPU
        double sum_cpu = 0.0;
        // CPU iterations
        #pragma omp parallel for simd reduction(+:sum_cpu)
        for (size_t i = ngpu; i < n; i++) {
            MAIN_BODY;
            sum_cpu = sum_cpu + y[i];
        }
        clock_gettime(CLOCK_MONOTONIC, &cpuend);
        // Wait for GPU task
        #pragma omp taskwait
    #endif
    clock_gettime(CLOCK_MONOTONIC, &end);
    time_main = get_time_s(start,end);
    #ifdef CGPU
        sum += sum_cpu;
        time_main_cpu = get_time_s(start,cpuend);
    #endif

    #if defined(GPU) || defined(CGPU)
        #pragma omp target exit data map(delete:x[:ngpu],y[:ngpu])
        #pragma acc exit data delete(x[0:ngpu],y[0:ngpu])
    #endif

    free(x);
    free(y);

    printf("Sum: %.15g\n", sum);

    printf("Time init: %g sec\n", time_init);
    printf("Time main: %g sec\n", time_main);
    #ifdef CGPU
        printf("Time main_cpu: %g sec\n", time_main_cpu);
    #endif
    return 0;
}
